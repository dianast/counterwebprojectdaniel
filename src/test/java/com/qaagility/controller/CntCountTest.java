package com.qaagility.controller;


import org.junit.Test;
import static org.junit.Assert.*;

public class  CntCountTest {
    @Test
    public void testAdd() throws Exception {
        int k = new CntCount().add(10,0);
        assertEquals("Add", Integer.MAX_VALUE, k);
    }

    @Test
    public void testDiv() throws Exception {
        int k = new CntCount().add(10,5);
        assertEquals("Sub", 2, k);
    }
}
